package main

import (
	"github.com/gin-gonic/gin"
)

// Purchase is a struct
type Purchase struct {
	Name string
}

func main() {
	setupServer().Run()
	// r := gin.Default()

	// r.GET("/ping", func(c *gin.Context) {
	// 	c.JSON(200, gin.H{"message": "pong"})
	// })

	// r.POST("/purchase", func(c *gin.Context) {
	// 	var purchase Purchase
	// 	c.BindJSON(&purchase)

	// 	c.JSON(200, gin.H{"name": fmt.Sprintf("%v", purchase.Name)})
	// })

	// r.Run()
}

// The engine with all endpoints is now extracted from the main function
func setupServer() *gin.Engine {
	r := gin.Default()

	r.GET("/ping", pingEndpoint)

	return r
}

func pingEndpoint(c *gin.Context) {
	c.JSON(200, gin.H{
		"message": "pong",
	})
}
